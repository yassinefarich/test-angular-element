const fs = require('fs-extra');
const concat = require('concat');

(async function build() {
  const files = [
    './dist/helloAngular/runtime.js',
    './dist/helloAngular/polyfills.js',
    './dist/helloAngular/main.js'
  ];

  await fs.ensureDir('elements');
  await concat(files, 'elements/element.js');
  await fs.copyFile(
    './dist/helloAngular/styles.css',
    'elements/styles.css'
  );
})();